import Vue from 'vue'
import Router from 'vue-router'
import Tab from '@/components/Tab'
import games from '@/components/games'
import com from '@/components/com'
Vue.use(Router)
export default new Router({
  routes: [
    {
      path: '/tab',
      name: 'Tab',
      component: Tab
    },
     {
      path: '/games',
      name: 'games',
      component: games
    }
    ,
     {
      path: '/com',
      name: 'com',
      component: com
    }
  ]
})
