import Vue from "vue";
import Vuex from "vuex";
import * as getters from "./getters.js";
Vue.use(Vuex)
const state={
	index:0
}
//变更的函数 改变state方法的集合
const mutations={
	changeIndex(state,index){
		state.index=index
	}
}
//mutations改变 触发更新事件
const actions={
	changeIndex({commit,state},index){
		commit("changeIndex",index)
	}
}
// 抛出实例
export default new Vuex.Store({
	state,
	actions,
	mutations,
	getters
})
